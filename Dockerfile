FROM gutocarvalho/ubuntu-systemd:latest
MAINTAINER Guto Carvalho "gutocarvalho@gmail.com"

ENV PUPPET_AGENT_VERSION="1.6.2" UBUNTU_CODENAME="xenial"

RUN apt-get update && \
    apt-get install -y wget=1.17.1-1ubuntu1 lsb-release=9.20160110ubuntu0.2 && \
    wget https://apt.puppetlabs.com/puppetlabs-release-pc1-"$UBUNTU_CODENAME".deb && \
    dpkg -i puppetlabs-release-pc1-"$UBUNTU_CODENAME".deb && \
    apt-get update && \
    apt-get install --no-install-recommends -y puppet-agent="$PUPPET_AGENT_VERSION"-1"$UBUNTU_CODENAME" && \
    apt-get install -y git vim wget rsync screen dnsutils netcat telnet elinks lynx bzip2 unzip tcpdump ccze htop traceroute && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV PATH=/opt/puppetlabs/server/bin:/opt/puppetlabs/puppet/bin:/opt/puppetlabs/bin:$PATH

CMD ["/sbin/init"]

COPY Dockerfile /
